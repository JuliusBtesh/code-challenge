//
//  HomeViewController.h
//  codechallenge
//
//  Created by Julius Btesh on 6/14/16.
//  Copyright © 2016 Julius Btesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;

// List of items to be taken from URL
@property (nonatomic, strong) NSMutableArray *items;

@end

