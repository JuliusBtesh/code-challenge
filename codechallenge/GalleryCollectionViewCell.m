//
//  GalleryCollectionViewCell.m
//  codechallenge
//
//  Created by Julius Btesh on 6/14/16.
//  Copyright © 2016 Julius Btesh. All rights reserved.
//

#import "GalleryCollectionViewCell.h"

#import "CCRect.h"

@implementation GalleryCollectionViewCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    const CGRect bounds = self.contentView.bounds;
    
    CGRect imageViewFrame = CGRectZero;
    if (_imageView && _imageView.image){
        imageViewFrame = CCRectMakeWithSizeCenteredInRect(CCSizeMin(_imageView.image.size, bounds.size),
                                                          bounds);
    }
    
    // Adapt content mode of _imageView to fit the image in bounds if the layout frame is smaller or center if it's bigger.
    if (!CGRectIsEmpty(imageViewFrame)) {
        if (CGRectContainsRect(CCRectMakeWithSize(_imageView.image.size), CCRectMakeWithSize(imageViewFrame.size))) {
            _imageView.contentMode = UIViewContentModeScaleAspectFit;
        } else {
            _imageView.contentMode = UIViewContentModeCenter;
        }
    }
    
    _imageView.frame = CGRectIntegral(imageViewFrame);
}

- (CCImageView *)imageView {
    if (!_imageView) {
        _imageView = [[CCImageView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:_imageView];
    }
    return _imageView;
}

@end
