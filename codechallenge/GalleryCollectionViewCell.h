//
//  GalleryCollectionViewCell.h
//  codechallenge
//
//  Created by Julius Btesh on 6/14/16.
//  Copyright © 2016 Julius Btesh. All rights reserved.
//

#import "CCImageView.h"

@interface GalleryCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) CCImageView *imageView;

@end
