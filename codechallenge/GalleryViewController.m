//
//  GalleryViewController.m
//  codechallenge
//
//  Created by Julius Btesh on 6/15/16.
//  Copyright © 2016 Julius Btesh. All rights reserved.
//

#import "GalleryViewController.h"
#import "CCImageView.h"

@interface GalleryViewController () {
    NSInteger _index;
    
    CCImageView *imageView;
}

@end

@implementation GalleryViewController

- (id)initWithIndex:(NSInteger)index {
    self = [super init];
    if (self) {
        _index = index;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(removeImage)];
    
    CGRect middleRect = CGRectMake(0, self.view.frame.size.height/2-self.view.frame.size.width/2, self.view.frame.size.width, self.view.frame.size.width);
    
    imageView = [[CCImageView alloc] initWithFrame:middleRect];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    [self.view addSubview:imageView];
    
    if (self.homeViewController) {
        if (self.homeViewController.items.count > 0) {
            [self loadImage];
        }
    }
    
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(loadNextImage) userInfo:nil repeats:YES];
}

- (void)setTitle {
    self.title = [NSString stringWithFormat:@"%ld/%lu", (long)(_index+1), (unsigned long)self.homeViewController.items.count];
}

// This will react to the Scheduled Timer
// Will add to index
- (void)loadNextImage {
    if (_index < self.homeViewController.items.count)
        _index++;
    [self loadImage];
}

// This will load the proper image to display
- (void)loadImage {
    NSString *urlString = @"";
    if (_index >= self.homeViewController.items.count) {
        _index = 0;
    }
    
    if ([self isListEmpty]) {
        _index = -1;
        [NSTimer cancelPreviousPerformRequestsWithTarget:self];
    } else {
        urlString = [[self.homeViewController.items objectAtIndex:_index] objectForKey:@"imageURL"];
    }
    [imageView setUrl:[NSURL URLWithString:urlString]];
    
    [self setTitle];
}

// This will remove the image from list
- (void)removeImage {
    if (_index < self.homeViewController.items.count)
        [self.homeViewController.items removeObjectAtIndex:_index];
    if (_index >= self.homeViewController.items.count)
        _index = self.homeViewController.items.count-1;
    [self loadImage];
}

// This will check if list is empty
- (BOOL)isListEmpty {
    return  self.homeViewController.items.count == 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
