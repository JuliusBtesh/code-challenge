//
//  GalleryViewController.h
//  codechallenge
//
//  Created by Julius Btesh on 6/15/16.
//  Copyright © 2016 Julius Btesh. All rights reserved.
//

#import "HomeViewController.h"

@interface GalleryViewController : UIViewController

@property (nonatomic, strong) HomeViewController *homeViewController;

- (id)initWithIndex:(NSInteger)index;

@end
