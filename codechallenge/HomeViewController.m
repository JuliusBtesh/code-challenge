//
//  HomeViewController.m
//  codechallenge
//
//  Created by Julius Btesh on 6/14/16.
//  Copyright © 2016 Julius Btesh. All rights reserved.
//

#import "HomeViewController.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "GalleryViewController.h"
#import "GalleryCollectionViewCell.h"

#define galleryCell @"GalleryCell"

#define numOfCol 4

@interface HomeViewController () {
    UIView *emptyView;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.title = @"Code Challenge";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRedo target:self action:@selector(loadFromJSON)];
    
    [self loadFromJSON];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    
    [self.collectionView registerClass:[GalleryCollectionViewCell class] forCellWithReuseIdentifier:galleryCell];
    
    [self.view addSubview:self.collectionView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.collectionView reloadData];
}

- (void)loadFromJSON {
    NSURL *url = [NSURL URLWithString:@"https://hinge-homework.s3.amazonaws.com/client/services/homework.json"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
        [self hideEmptyPage];
        self.items = [NSMutableArray arrayWithArray:responseObject];
        
        [self.collectionView reloadData];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self showEmptyPage];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createEmptyView {
    if (!emptyView) {
        emptyView = [[UIView alloc] initWithFrame:self.view.frame];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:emptyView.frame];
        [imageView setImage:[UIImage imageNamed:@"nothing"]];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        
        [emptyView addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, emptyView.frame.size.height-160, emptyView.frame.size.width-20, 80)];
        [label setNumberOfLines:2];
        [label setText:@"Nothing to see here\nNo internet connection found"];
        [label setTextAlignment:NSTextAlignmentCenter];
        
        [emptyView addSubview:label];
        
        [self.view addSubview:emptyView];
    }
}

- (void)showEmptyPage {
    [self createEmptyView];
    emptyView.alpha = 1;
}

- (void)hideEmptyPage {
    [self createEmptyView];
    emptyView.alpha = 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GalleryCollectionViewCell *cell = (GalleryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:galleryCell forIndexPath:indexPath];

    NSString *urlString = [[self.items objectAtIndex:indexPath.row] objectForKey:@"imageURL"];
    
    [cell.imageView setUrl:[NSURL URLWithString:urlString]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    GalleryViewController *gvc = [[GalleryViewController alloc] initWithIndex:indexPath.row];
    gvc.homeViewController = self;
    [self.navigationController pushViewController:gvc animated:YES];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat value = self.view.frame.size.width/numOfCol+1;
    return CGSizeMake(value, value);
}

@end
