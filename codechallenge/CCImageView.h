//
//  CCImageView.h
//  codechallenge
//
//  Created by Julius Btesh on 6/14/16.
//  Copyright © 2016 Julius Btesh. All rights reserved.
//

#import <JMImageCache/UIImageView+JMImageCache.h>

NS_ASSUME_NONNULL_BEGIN

/**
 An image view that downloads and displays remote image
 */
@interface CCImageView : UIImageView

@property (nullable, nonatomic, strong) NSURL *url;

@end

NS_ASSUME_NONNULL_END
