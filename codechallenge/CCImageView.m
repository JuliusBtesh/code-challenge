//
//  CCImageView.m
//  codechallenge
//
//  Created by Julius Btesh on 6/14/16.
//  Copyright © 2016 Julius Btesh. All rights reserved.
//
#import "CCImageView.h"

@implementation CCImageView

#pragma mark -
#pragma mark Accessors

- (void)setUrl:(NSURL *)url {
    _url = url;
    [self setImageWithURL:url
                        placeholder:[UIImage imageNamed:@"placeholder"]];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [self.layer addAnimation:transition forKey:nil];
}

@end
