# Code Challenge #

### Quick Summary ###

Very simple iOS Project:
App will read from JSON file and attempt to load images into a collection view.
Each cell will asynchronously load the corresponding images into place.
Each cell is selectable and will push to a new view displaying the selected image with index and list count.
Every two seconds the image will change to the next image in the list.

### Libraries Being Used ###

*These libraries help with retrieving the JSON file and asynchronous loading and caching of images*


AFNetworking


JMImageCache